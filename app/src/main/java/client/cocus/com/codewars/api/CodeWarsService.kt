package client.cocus.com.codewars.api

import client.cocus.com.codewars.model.*
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import rx.Observable


interface CodeWarsService {

    @GET("users/{username}/code-challenges/authored")
    fun getChallengeAuthored(@Path("username") username: String,
                              @Query("access_key") key: String): Observable<Authored>

    @GET("users/{username}/code-challenges/completed")
    fun getChallengeCompleted(@Path("username") username: String,
                              @Query("page") page: Int, @Query("access_key") key: String): Observable<Completed>

    @GET("users/{username}")
    fun getUser(@Path("username") username: String,
                @Query("access_key") key: String): Observable<Users>

}
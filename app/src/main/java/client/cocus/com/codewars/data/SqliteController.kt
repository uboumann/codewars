package client.cocus.com.codewars.data

import android.content.ContentValues
import android.content.Context

class SqliteController(internal var context: Context) {

    val sqliteHelper: SqliteHelper

    init {
        sqliteHelper = SqliteHelper(context)
    }

    fun saveUserData(username: String): Boolean {
        val values = ContentValues()
        values.put(SqliteTable.USER_USERNAME, username)
        return sqliteHelper.insertData(SqliteTable.TABLE_USER, values)
    }

    fun getUsers(): MutableList<String>? {
        return sqliteHelper.getListUser()
    }

}
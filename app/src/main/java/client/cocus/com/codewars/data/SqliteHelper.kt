package client.cocus.com.codewars.data

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log

class SqliteHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(SqliteTable.DB_USER)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL("DROP TABLE IF EXISTS " + SqliteTable.DB_USER)
        onCreate(db)
    }

    fun insertData(table: String, values: ContentValues): Boolean {
        val db = this.writableDatabase
        val result = db.insert(table,
                null, values)
        if (result.toInt() == -1) {
            Log.d(TAG, "failed to save data!")
            return false
        } else {
            Log.d(TAG, "save data successful")
            return true
        }
    }

    fun getListUser(): MutableList<String>? {

        var user: MutableList<String>
        val db = this.readableDatabase
        val cursor = db.rawQuery("select * from "+SqliteTable.TABLE_USER+" LIMIT 5",null)

        user = arrayListOf()
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast) {
                val name:String = cursor.getString(cursor.getColumnIndex(SqliteTable.USER_USERNAME))

                user.add(name)
                cursor.moveToNext()
            }
        }

        return user

    }

    companion object {

        private val DATABASE_NAME = "user"
        private val DATABASE_VERSION = 1
        private val TAG = SqliteHelper::class.java.simpleName
    }

}
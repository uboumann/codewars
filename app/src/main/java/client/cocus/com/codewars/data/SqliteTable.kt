package client.cocus.com.codewars.data

object SqliteTable {

    val TABLE_USER = "user"
    val USER_ID = "id"
    val USER_USERNAME = "username"

    val DB_USER = ("CREATE TABLE " + TABLE_USER + "(" + USER_ID + " INTEGER PRIMARY KEY, "
            + USER_USERNAME + " TEXT)")

}
package client.cocus.com.codewars.flow.authored

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import client.cocus.com.codewars.R
import client.cocus.com.codewars.model.DataAuthored
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_list.*


class AuthoredAdapter(private val authoreds: MutableList<DataAuthored>,
                      private val onClick: (DataAuthored) -> Unit)
    : RecyclerView.Adapter<AuthoredAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(authoreds[position])
    }

    override fun getItemCount(): Int = authoreds.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return LayoutInflater.from(parent.context)
                .inflate(R.layout.item_list, parent, false).let {
                    ViewHolder(it, onClick)
                }
    }

    class ViewHolder(override val containerView: View, private val onClick: (DataAuthored) -> Unit) : RecyclerView.ViewHolder(containerView), LayoutContainer {

        fun bindData(dataAuthored: DataAuthored) {
            with(dataAuthored) {
                titleItem.text = dataAuthored.name
                containerView.setOnClickListener { onClick(this) }
            }
        }
    }


    fun addAuthored(newAuthored: List<DataAuthored>) {
        authoreds.addAll(newAuthored)
    }
}
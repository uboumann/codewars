package client.cocus.com.codewars.flow.authored

import client.cocus.com.codewars.model.Authored
import client.cocus.com.codewars.mvp.BaseMvpPresenter
import client.cocus.com.codewars.mvp.BaseMvpView


object AuthoredContract {

    interface View : BaseMvpView {
        fun showAuthored(authored: Authored)

        fun hideLoading()

        fun showLoading()

    }

    interface Presenter : BaseMvpPresenter<View> {
        fun loadListAuthored(username: String)

    }


}

package client.cocus.com.codewars.flow.authored

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import client.cocus.com.codewars.R
import client.cocus.com.codewars.flow.detailAuthored.DetailAuthoredActivity
import client.cocus.com.codewars.model.Authored
import client.cocus.com.codewars.model.DataAuthored
import client.cocus.com.codewars.mvp.BaseMvpFragment
import kotlinx.android.synthetic.main.list_view.*
import java.util.ArrayList

class AuthoredFragment: BaseMvpFragment<AuthoredContract.View,
        AuthoredContract.Presenter>(), AuthoredContract.View {

    private var mAdapter: AuthoredAdapter? = null
    val USERNAME = "username"
    lateinit var username:String

    override var mPresenter: AuthoredContract.Presenter = AuthoredPresenter()

    override fun showAuthored(authored: Authored) {
        hideLoading()
        mAdapter?.addAuthored(authored.data)
        mAdapter?.notifyDataSetChanged()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.list_view, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val arguments = arguments
        username = arguments!!.getString(USERNAME)
        setUpRecyclerView()
        mPresenter.loadListAuthored(username)
    }

    override fun getContext(): Context {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun setUpRecyclerView() {

        mAdapter = AuthoredAdapter(ArrayList<DataAuthored>(), {
            startActivity(DetailAuthoredActivity.newIntent(activity!!.applicationContext, it))
        })

        val linearLayoutManager: LinearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)

        recyclerView.layoutManager = linearLayoutManager
        recyclerView.adapter = mAdapter

    }

    override fun hideLoading() {
        progress_bar.visibility = View.GONE
    }

    override fun showLoading() {
        progress_bar.visibility = View.VISIBLE
    }

}
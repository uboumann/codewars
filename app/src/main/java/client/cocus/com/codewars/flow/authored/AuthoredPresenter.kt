package client.cocus.com.codewars.flow.authored

import android.util.Log
import client.cocus.com.codewars.api.GeneralErrorHandler
import client.cocus.com.codewars.manager.ApiManager
import client.cocus.com.codewars.mvp.BaseMvpPresenterImpl
import rx.functions.Action1

class AuthoredPresenter : BaseMvpPresenterImpl<AuthoredContract.View>(),
        AuthoredContract.Presenter {

    companion object {
        private val KEY = "PuanzrNoiFox6LgLTz2F"
    }

    override fun loadListAuthored(username: String) {
        mView?.showLoading()
        ApiManager.loadAuthoredChallenge(username, KEY)
                .doOnError { Log.e("", it.toString()) }
                .subscribe(Action1 { mView?.showAuthored(it) },
                        GeneralErrorHandler(mView, true,
                                { throwable, errorBody, isNetwork -> mView?.hideLoading() }))
    }
}
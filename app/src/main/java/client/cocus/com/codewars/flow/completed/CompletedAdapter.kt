package client.cocus.com.codewars.flow.completed

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import client.cocus.com.codewars.R
import kotlinx.android.synthetic.main.item_list.*
import client.cocus.com.codewars.model.DataCompleted
import kotlinx.android.extensions.LayoutContainer

class CompletedAdapter(private val completed: MutableList<DataCompleted>,
                       private val onClick: (DataCompleted) -> Unit)
    : RecyclerView.Adapter<CompletedAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(completed[position])
    }

    override fun getItemCount(): Int = completed.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return LayoutInflater.from(parent.context)
                .inflate(R.layout.item_list, parent, false).let {
                    ViewHolder(it, onClick)
                }
    }

    class ViewHolder(override val containerView: View, private val onClick: (DataCompleted) -> Unit) : RecyclerView.ViewHolder(containerView), LayoutContainer {

        fun bindData(dataCompleted: DataCompleted) {
            with(dataCompleted) {
                titleItem.text = dataCompleted.name
                containerView.setOnClickListener { onClick(this) }
            }
        }
    }


    fun addCompleted(listCompleted: List<DataCompleted>) {
        completed.addAll(listCompleted)
    }
}
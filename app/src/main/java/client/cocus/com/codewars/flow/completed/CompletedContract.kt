package client.cocus.com.codewars.flow.completed

import client.cocus.com.codewars.model.Completed
import client.cocus.com.codewars.mvp.BaseMvpPresenter
import client.cocus.com.codewars.mvp.BaseMvpView

object CompletedContract {

    interface View : BaseMvpView {
        fun showCompleted(completed: Completed)
        
        fun hideLoading()

        fun showLoading()
    }

    interface Presenter : BaseMvpPresenter<View> {
        fun loadListCompleted(username: String, page: Int)

    }


}

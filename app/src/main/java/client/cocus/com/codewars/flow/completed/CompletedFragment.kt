package client.cocus.com.codewars.flow.completed

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import client.cocus.com.codewars.R
import client.cocus.com.codewars.flow.detailCompleted.DetailCompletedActivity
import client.cocus.com.codewars.model.Completed
import client.cocus.com.codewars.model.DataCompleted
import client.cocus.com.codewars.mvp.BaseMvpFragment
import kotlinx.android.synthetic.main.list_view.*
import java.util.ArrayList
import android.support.v7.widget.RecyclerView
import android.util.Log


class CompletedFragment : BaseMvpFragment<CompletedContract.View,
        CompletedContract.Presenter>(), CompletedContract.View {

    private var mAdapter: CompletedAdapter? = null
    val USERNAME = "username"
    var qtdPage: Int = 0
    var lastPage: Int = 1
    var isLoading = false
    lateinit var username:String

    override var mPresenter: CompletedContract.Presenter = CompletedPresenter()

    override fun showCompleted(completed: Completed) {
        hideLoading()
        isLoading = false
        qtdPage = completed.totalPages.toInt()
        mAdapter?.addCompleted(completed.data)
        mAdapter?.notifyDataSetChanged()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.list_view, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val arguments = arguments
        username = arguments!!.getString(USERNAME)
        setUpRecyclerView()
        mPresenter.loadListCompleted(username, 0)
    }

    override fun getContext(): Context {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun setUpRecyclerView() {

        mAdapter = CompletedAdapter(ArrayList<DataCompleted>(), {
            startActivity(DetailCompletedActivity.newIntent(activity!!.applicationContext, it))
        })

        val linearLayoutManager: LinearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)

        recyclerView.layoutManager = linearLayoutManager
        recyclerView.adapter = mAdapter

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                if (dy > 0) {

                    val visibleItemCount = linearLayoutManager.childCount
                    val totalItemCount = linearLayoutManager.itemCount
                    val pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition()


                    if (visibleItemCount + pastVisiblesItems >= totalItemCount) {
                        if(!isLoading) {
                            Log.e("qtd-", qtdPage.toString()+" : "+lastPage)
                            if(qtdPage > lastPage) {
                                isLoading = true
                                mPresenter.loadListCompleted(username, lastPage)
                                lastPage += 1
                            }
                        }
                    }

                }
            }
        })
    }

    override fun hideLoading() {
        progress_bar.visibility = View.GONE
    }

    override fun showLoading() {
        progress_bar.visibility = View.VISIBLE
    }

}
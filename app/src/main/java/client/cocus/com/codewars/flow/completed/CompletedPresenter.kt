package client.cocus.com.codewars.flow.completed

import android.util.Log
import client.cocus.com.codewars.api.GeneralErrorHandler
import client.cocus.com.codewars.manager.ApiManager
import client.cocus.com.codewars.mvp.BaseMvpPresenterImpl
import rx.functions.Action1

class CompletedPresenter : BaseMvpPresenterImpl<CompletedContract.View>(), CompletedContract.Presenter {

    companion object {
        private val KEY = "PuanzrNoiFox6LgLTz2F"
    }

    override fun loadListCompleted(userName: String, page: Int) {
        mView?.showLoading()
        ApiManager.loadCompletedChallenge(userName, page, KEY)
                .doOnError { mView?.showMessage(it.toString()) }
                .subscribe(Action1 {mView?. showCompleted(it) },
                        GeneralErrorHandler(mView, true,
                                { throwable, errorBody, isNetwork -> mView?.hideLoading() }))
    }
}
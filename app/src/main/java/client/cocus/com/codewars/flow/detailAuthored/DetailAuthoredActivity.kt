package client.cocus.com.codewars.flow.detailAuthored

import android.content.Context
import android.content.Intent
import android.os.Bundle
import client.cocus.com.codewars.R
import client.cocus.com.codewars.model.DataAuthored
import client.cocus.com.codewars.mvp.BaseMvpActivity
import kotlinx.android.synthetic.main.activity_detail_authored.*

class DetailAuthoredActivity : BaseMvpActivity<DetailAuthoredContract.View,
        DetailAuthoredContract.Presenter>(),
        DetailAuthoredContract.View {

    override var mPresenter: DetailAuthoredContract.Presenter = DetailAuthoredPresenter()
    var tags: String = ""
    var languages: String = ""

    companion object {
        var listTags: List<String>? = null
        var listLanguages: List<String>? = null
        lateinit var authored: DataAuthored

        fun newIntent(context: Context, dataAuthored: DataAuthored): Intent =
                Intent(context, DetailAuthoredActivity::class.java).apply {
                    authored = dataAuthored
                }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_authored)

        dataName.text = authored?.name
        dataDescription.text = authored?.description
        dataRank.text = authored?.rank
        dataRankName.text = authored?.rankName
        listTags = authored?.tags
        listLanguages = authored?.languages

        for (i in listTags!!.indices) {
            tags += "- " + listTags!![i] + "\n"
        }

        dataTags.text = tags

        for (i in listLanguages!!.indices) {
            languages += "- " + listLanguages!![i] + "\n"
        }
        dataLanguages.text = languages

    }

}
package client.cocus.com.codewars.flow.detailAuthored

import client.cocus.com.codewars.mvp.BaseMvpPresenter
import client.cocus.com.codewars.mvp.BaseMvpView

object DetailAuthoredContract {

    interface View : BaseMvpView {

    }

    interface Presenter : BaseMvpPresenter<View> {
        fun verifyIsNull(value: String):String
    }


}
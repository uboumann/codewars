package client.cocus.com.codewars.flow.detailCompleted

import android.content.Context
import android.content.Intent
import android.os.Bundle
import client.cocus.com.codewars.R
import client.cocus.com.codewars.flow.detailAuthored.DetailAuthoredContract
import client.cocus.com.codewars.flow.detailAuthored.DetailAuthoredPresenter
import client.cocus.com.codewars.model.DataCompleted
import client.cocus.com.codewars.mvp.BaseMvpActivity
import kotlinx.android.synthetic.main.activity_detail_completed.*

class DetailCompletedActivity : BaseMvpActivity<DetailAuthoredContract.View,
        DetailAuthoredContract.Presenter>(),
        DetailAuthoredContract.View {

    override var mPresenter: DetailAuthoredContract.Presenter = DetailAuthoredPresenter()
    var stringChallenges: String = ""

    companion object {
        var listChallenges: List<String>? = null
        lateinit var completed: DataCompleted

        fun newIntent(context: Context, dataCompleted: DataCompleted): Intent =
                Intent(context, DetailCompletedActivity::class.java).apply {
                    completed = dataCompleted
                    listChallenges = dataCompleted.completedLanguages
                }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_completed)
        dataName.text = completed?.name
        dataSlug.text = completed?.slug

        for (i in listChallenges!!.indices) {
            stringChallenges += "- " + listChallenges!![i] + "\n"
        }
        dataChallengesCompleted.text = stringChallenges
    }

}
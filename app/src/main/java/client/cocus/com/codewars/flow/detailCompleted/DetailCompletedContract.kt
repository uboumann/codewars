package client.cocus.com.codewars.flow.detailCompleted

import client.cocus.com.codewars.mvp.BaseMvpPresenter
import client.cocus.com.codewars.mvp.BaseMvpView

object DetailCompletedContract {

    interface View : BaseMvpView {

    }

    interface Presenter : BaseMvpPresenter<View> {
        fun loadDetail(name: String)
    }


}
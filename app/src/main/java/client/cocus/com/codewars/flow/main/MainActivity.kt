package client.cocus.com.codewars.flow.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import client.cocus.com.codewars.R
import client.cocus.com.codewars.flow.authored.AuthoredFragment
import client.cocus.com.codewars.flow.completed.CompletedFragment
import client.cocus.com.codewars.mvp.BaseMvpActivity
import kotlinx.android.synthetic.main.activity_main.*
import android.R.attr.fragment



class MainActivity : BaseMvpActivity<MainContract.View,
        MainContract.Presenter>(),
        MainContract.View {

    override var mPresenter: MainContract.Presenter = MainPresenter()

    val completedFragment = CompletedFragment()
    val authoredFragment = AuthoredFragment()



    companion object {
        private const val USERNAME = "username"
        var username:String = ""

        fun newIntent(context: Context, name: String): Intent =
                Intent(context, MainActivity::class.java).apply {
                    username = name
                }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navigation.selectedItemId = R.id.action_search
        replaceFragment(completedFragment)

        navigation.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.action_search -> replaceFragment(completedFragment)
                R.id.action_settings -> replaceFragment(authoredFragment)
            }
            true
        }

    }

    private fun replaceFragment(fragment: Fragment){
        val arguments = Bundle()
        arguments.putString(USERNAME, username)
        fragment.arguments = arguments
        supportFragmentManager.beginTransaction().replace(R.id.frameLayout, fragment).commit()
    }

    override fun onStart() {
        super.onStart()
        navigation.selectedItemId = 0
    }


}
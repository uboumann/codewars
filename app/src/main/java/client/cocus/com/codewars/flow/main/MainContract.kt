package client.cocus.com.codewars.flow.main

import client.cocus.com.codewars.mvp.BaseMvpPresenter
import client.cocus.com.codewars.mvp.BaseMvpView


object MainContract {

    interface View : BaseMvpView {

    }

    interface Presenter : BaseMvpPresenter<View> {

    }


}
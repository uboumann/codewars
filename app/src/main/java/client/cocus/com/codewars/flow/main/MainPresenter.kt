package client.cocus.com.codewars.flow.main

import client.cocus.com.codewars.mvp.BaseMvpPresenterImpl

class MainPresenter : BaseMvpPresenterImpl<MainContract.View>(),
        MainContract.Presenter {

}
package client.cocus.com.codewars.flow.user

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import client.cocus.com.codewars.R
import client.cocus.com.codewars.flow.main.MainActivity
import client.cocus.com.codewars.model.Users
import client.cocus.com.codewars.mvp.BaseMvpActivity
import kotlinx.android.synthetic.main.activity_user.*

class UserActivity : BaseMvpActivity<UserContract.View,
        UserContract.Presenter>(),
        UserContract.View {

    override var mPresenter: UserContract.Presenter = UserPresenter()
    private var mAdapter: UserAdapter? = null

    override fun showUser(user: Users) {
        layoutContent.visibility = View.VISIBLE
        progressBar.visibility = View.GONE
        with(user) {
            mPresenter.saveFindList(this@UserActivity, user)
            nameUser.text = getString(R.string.result_username, username)
            nameUser.setOnClickListener {
                startActivity(MainActivity.newIntent(this@UserActivity, user.username))
            }
            if(mPresenter.getFindList(this@UserActivity).isNotEmpty() || mPresenter.getFindList(this@UserActivity) != null) {
                showList()
            }
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)


        layoutContent.visibility = View.VISIBLE
        progressBar.visibility = View.GONE

        buttonFind.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            mPresenter.loadUser(inputFindUser.text.toString())
        }
        toolbar.title = getString(R.string.title_user)

    }

    override fun hideLoading() {
        progressBar.visibility = View.GONE
    }

    override fun showError() {
        Toast.makeText(this, "User not found", Toast.LENGTH_LONG ).show()
    }

    override fun onResume() {
        super.onResume()
        showList()
    }

    fun showList() {
        if(mPresenter.getFindList(this).isNotEmpty() || mPresenter.getFindList(this) != null) {

            mAdapter = UserAdapter(ArrayList<String>(), {
                startActivity(MainActivity.newIntent(this@UserActivity, it))
            })

            mAdapter?.addAuthored(mPresenter.getFindList(this@UserActivity))
            mAdapter?.notifyDataSetChanged()


            val linearLayoutManager: LinearLayoutManager = LinearLayoutManager(this@UserActivity, LinearLayoutManager.VERTICAL, false)

            recyclerViewLast.layoutManager = linearLayoutManager
            recyclerViewLast.adapter = mAdapter
        }
    }

}
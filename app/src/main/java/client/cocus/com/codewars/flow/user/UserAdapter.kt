package client.cocus.com.codewars.flow.user

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import client.cocus.com.codewars.R
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_list.*

class UserAdapter(private val users: MutableList<String>,
                  private val onClick: (username: String) -> Unit)
    : RecyclerView.Adapter<UserAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(users[position])
    }

    override fun getItemCount(): Int = users.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return LayoutInflater.from(parent.context)
                .inflate(R.layout.item_list, parent, false).let {
                    ViewHolder(it, onClick)
                }
    }

    class ViewHolder(override val containerView: View, private val onClick: (username: String) -> Unit) : RecyclerView.ViewHolder(containerView), LayoutContainer {

        fun bindData(username: String) {
            with(username) {
                titleItem.text = username
                containerView.setOnClickListener { onClick(this) }
            }
        }
    }

    fun addAuthored(userList: MutableList<String>) {
        users.addAll(userList)
    }
}
package client.cocus.com.codewars.flow.user

import android.content.Context
import client.cocus.com.codewars.model.Users
import client.cocus.com.codewars.mvp.BaseMvpPresenter
import client.cocus.com.codewars.mvp.BaseMvpView

object UserContract {

    interface View : BaseMvpView {
        fun showUser(user: Users)

        fun hideLoading()

        fun showError()
    }

    interface Presenter : BaseMvpPresenter<View> {
        fun loadUser(name: String)
        fun saveFindList(context: Context, users: Users)
        fun getFindList(context: Context):MutableList<String>
    }


}
package client.cocus.com.codewars.flow.user

import android.content.Context
import client.cocus.com.codewars.api.GeneralErrorHandler
import client.cocus.com.codewars.manager.ApiManager
import client.cocus.com.codewars.model.Users
import client.cocus.com.codewars.mvp.BaseMvpPresenterImpl
import rx.functions.Action1
import client.cocus.com.codewars.data.SqliteController

class UserPresenter : BaseMvpPresenterImpl<UserContract.View>(),
        UserContract.Presenter {

    lateinit var sqliteController: SqliteController

    override fun saveFindList(context: Context, users: Users) {

        sqliteController = SqliteController(context)

        sqliteController.saveUserData(users.username)

    }

    override fun getFindList(context: Context):MutableList<String> {
        sqliteController = SqliteController(context)
        return sqliteController.getUsers()!!
    }

    companion object {
        private const val KEY = "PuanzrNoiFox6LgLTz2F"
    }

    override fun loadUser(name: String) {
        ApiManager.loadUser(name, KEY)
                .doOnError { mView?.showError()}
                .subscribe(Action1 { mView?.showUser(it) },
                        GeneralErrorHandler(mView, true,
                                { throwable, errorBody, isNetwork -> mView?.hideLoading() }))
    }
}
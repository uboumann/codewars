package client.cocus.com.codewars.model

data class Authored(val totalPages: String, val totalItems: String, val data: List<DataAuthored>)
package client.cocus.com.codewars.model

data class Completed(val totalPages: String, val totalItems: String, val data: List<DataCompleted>)
package client.cocus.com.codewars.model

data class DataAuthored(val id: String?, val name: String?, val description: String?, val rank: String? = null, val rankName: String, val tags: List<String>?, val languages: List<String>?)
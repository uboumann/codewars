package client.cocus.com.codewars.model

data class DataCompleted(val id: String, val name: String, val slug: String, val completedLanguages: List<String>)
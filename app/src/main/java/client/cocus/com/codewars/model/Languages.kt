package client.cocus.com.codewars.model

import org.json.JSONObject

data class Languages(val rank: String, val name: String, val color: String, val score: String)
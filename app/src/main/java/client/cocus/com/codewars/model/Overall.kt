package client.cocus.com.codewars.model

data class Overall(val rank: String, val name: String, val color: String, val score: String)
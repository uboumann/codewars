package client.cocus.com.codewars.model

data class Users(val name: String, val username: String, val honor: String, val leaderboardPosition: String, val ranks: Ranks)